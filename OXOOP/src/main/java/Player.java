
public class Player {

    private char name;
    private int win;
    private int lose;
    private int draw;
    public Player() {
        this.name = name;
        win = 0;
        lose = 0;
        draw = 0;
    }

    public Player(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public void setWin() {
        win++;
    }

    public void setLose() {
        lose++;
    }

    public void setDraw() {
        draw++;
    }

}
