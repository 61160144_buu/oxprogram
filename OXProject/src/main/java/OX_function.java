import java.util.Scanner;

public class OX_function {
	static char player = 'O';
	static char[][] board = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	static Scanner kb;
	static boolean win = false;
	static int count = 0;

	public static void showWelcome() {
		System.out.println("Welcome to OX Game");
	}

	public static void showTurn() {
		System.out.println(player + " Turn");
	}

	public static void showInput() {
		System.out.print("Please input row, col :");
	}

	public static void showWrongInput() {
		System.out.println("Wrong Position input again!");
	}

	public static void showEnd() {
		System.out.println("End Game...");
	}

	public static void input(int getrow, int getcol) throws Exception {

		if (board[getrow - 1][getcol - 1] == '-') {
			board[getrow - 1][getcol - 1] = player;
		} else {
			throw new Exception();
		}
	}

	public static void switchPlayer() {

		if (player == 'X') {
			player = 'O';
		} else {
			player = 'X';
		}
	}

	public static void showTable() {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				System.out.print(" " + board[i][j] + " ");
			}
			System.out.println("");
		}
	}

	public static void showWin() {
		System.out.println("Player"+player + " Win!!");
	}

	public static void showDraw() {
		System.out.println("Play Draw!!");
	}

	public static boolean checkWin() {
		if (checkRow() || checkCol() || checkX1() || checkX2()) {
			showWin();
			win = true;
			return win;
		}
		if (count == 9) {
			showDraw();
			win = true;
			return win;
		}
		return win;
	}
	public static boolean checkRow() {
		for (int row = 0; row < board.length; row++) {
			if (board[row][0] == board[row][1] && board[row][1] == board[row][2] && board[row][0] != '-') {
				return true;
			}
		}
		return false;
	}
	public static boolean checkCol() {
		for (int col = 0; col < board[0].length; col++) {
			if (board[0][col] == board[1][col] && board[1][col] == board[2][col] && board[0][col] != '-') {
				return true;
			}
		}
		return false;
	}

	public static boolean checkX1() {

		if ((board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-')) {
			return true;
		}
		return false;
	}

	public static boolean checkX2() {
		if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != '-') {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		kb = new Scanner(System.in);
		showWelcome();
		showTable();
		while (true) {
			try {
				showTurn();
				showInput();
				int row = kb.nextInt();
				int col = kb.nextInt();
				input(row, col);
			} catch (Exception e) {
				showWrongInput();
				continue;
			}
			count++;
			showTable();
			if (checkWin() == true) {
				showEnd();
				break;
			}
			switchPlayer();
		}
	}
}