
import java.util.Scanner;

public class OX_GoodFunction {

    private static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private static void showTurn(char player) {
        System.out.println(player + " Turn");
    }

    private static void showTable(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(" " + board[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void showInput() {
        System.out.print("Please input row, col :");
    }

    private static void input(char[][] board, char player) {
        Scanner kb = new Scanner(System.in);
        try {
            int row = kb.nextInt();
            int col = kb.nextInt();
            setTable(row, col, board, player);
        } catch (Exception e) {
            showWrongInput();
        }
    }

    private static void setTable(int row, int col, char[][] board, char player) throws Exception {
        if (board[row - 1][col - 1] == '-') {
            board[row - 1][col - 1] = player;
        } else {
            throw new Exception();
        }
    }

    private static void showWrongInput() {
        System.out.println("Wrong Position input again!");
    }

    private static boolean checkWin(boolean win, char[][] board, char player) {
        if (checkRow(board) || checkCol(board) || checkX1(board) || checkX2(board)) {
            showWin(player);
            win = true;
            return win;
        }
        return win;
    }

    private static boolean checkRow(char[][] board) {
        for (int row = 0; row < board.length; row++) {
            if (board[row][0] == board[row][1] && board[row][1] == board[row][2] && board[row][0] != '-') {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(char[][] board) {
        for (int col = 0; col < board[0].length; col++) {
            if (board[0][col] == board[1][col] && board[1][col] == board[2][col] && board[0][col] != '-') {
                return true;
            }
        }
        return false;
    }

    private static boolean checkX1(char[][] board) {

        if ((board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-')) {
            return true;
        }
        return false;
    }

    private static boolean checkX2(char[][] board) {
        if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != '-') {
            return true;
        }
        return false;
    }

    private static void showWin(char player) {
        System.out.println("Player " + player + " Win!!");
    }

    private static boolean checkDraw(int count, boolean draw) {
        if (count == 9) {
            showDraw();
            draw = true;
            return draw;
        }
        return draw;
    }

    private static void showDraw() {
        System.out.println("Play Draw!!");
    }

    private static void showEnd() {
        System.out.println("End Game...");
    }

    private static char switchPlayer(char player) {

        if (player == 'X') {
            return 'O';

        } else {
            return 'X';

        }
    }

    public static void main(String[] args) throws Exception {
        char player = 'O';
        char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        int count = 0;
        boolean win = false;
        boolean draw = false;

        showWelcome();
        showTable(board);
        while (true) {
            showTurn(player);
            showInput();
            input(board, player);
            count++;
            showTable(board);
            if (checkWin(win, board, player) == true) {
                showEnd();
                break;
            } else if (checkDraw(count, draw) == true) {
                showEnd();
                break;
            }
            player = switchPlayer(player);

        }

    }

}
